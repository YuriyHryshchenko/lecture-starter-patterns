import type { Server, Socket } from 'socket.io'

import { ListEvent } from '../common/enums'
import { List } from '../data/models/list'
import { SocketHandler } from './socket.handler'
import * as path from 'node:path'
import type { Database } from '../data/database'
import { Logger } from '../logging/logger'
import { FileLoggerSubscriber } from '../logging/subcribers/file-logger.subscriber'
import { ConsoleLoggerSubscriber } from '../logging/subcribers/console-logger.subscriber'
import { LogLevel } from '../common/enums/log-levell.enum'
import type { ReorderServiceProxy } from '../proxies/reorder-service.proxy'

const logFilePath = path.join(__dirname, '..', '..', 'logs', 'listsLog.txt')

export class ListHandler extends SocketHandler {
  private logger: Logger = new Logger()
  private fileLoggerSubscriber: FileLoggerSubscriber = new FileLoggerSubscriber(logFilePath)
  private consoleLoggerSubscriber: ConsoleLoggerSubscriber = new ConsoleLoggerSubscriber()

  constructor(io: Server, db: Database, reorderService: ReorderServiceProxy) {
    super(io, db, reorderService)
    this.logger.subscribe(this.fileLoggerSubscriber)
    this.logger.subscribe(this.consoleLoggerSubscriber)
  }

  public handleConnection(socket: Socket): void {
    socket.on(ListEvent.CREATE, this.createList.bind(this))
    socket.on(ListEvent.GET, this.getLists.bind(this))
    socket.on(ListEvent.REORDER, this.reorderLists.bind(this))
    socket.on(ListEvent.DELETE, this.deleteList.bind(this))
    socket.on(ListEvent.RENAME, this.renameList.bind(this))
  }

  private getLists(callback: (cards: List[]) => void): void {
    callback(this.db.getData())
  }

  private reorderLists(sourceIndex: number, destinationIndex: number): void {
    try {
      const lists = this.db.getData()
      const reorderedLists = this.reorderService.reorder(lists, sourceIndex, destinationIndex)
      this.db.setData(reorderedLists)
      this.logger.log(LogLevel.INFO, `Lists reordered`)

      this.updateLists()
    } catch (error) {
      this.logger.log(LogLevel.ERROR, `Error reordering lists: ${error.message}`)
    }
  }

  private createList(name: string): void {
    try {
      const lists = this.db.getData()
      const newList = new List(name)
      this.db.setData(lists.concat(newList))
      this.logger.log(LogLevel.INFO, `List ${name} created`)

      this.updateLists()
    } catch (error) {
      this.logger.log(LogLevel.ERROR, `Error creating list: ${error.message}`)
    }
  }

  private deleteList(listId: string): void {
    try {
      const lists = this.db.getData()
      const updatedLists = lists.filter(list => list.id !== listId)
      this.db.setData(updatedLists)
      this.logger.log(LogLevel.INFO, `List ${listId} deleted`)

      this.updateLists()
    } catch (error) {
      this.logger.log(LogLevel.ERROR, `Error deleting list: ${error.message}`)
    }
  }

  private renameList(listId: string, name: string): void {
    try {
      const lists = this.db.getData()
      const updatedLists = lists.map(list => (list.id === listId ? ({ ...list, name } as List) : list))
      this.db.setData(updatedLists)
      this.logger.log(LogLevel.INFO, `List ${listId} renamed to ${name}`)

      this.updateLists()
    } catch (error) {
      this.logger.log(LogLevel.ERROR, `Error renaming list: ${error.message}`)
    }
  }
}
