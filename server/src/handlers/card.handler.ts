import type { Server, Socket } from 'socket.io'

import { CardEvent } from '../common/enums'
import { Card } from '../data/models/card'
import { SocketHandler } from './socket.handler'
import { ConsoleLoggerSubscriber } from '../logging/subcribers/console-logger.subscriber'
import { FileLoggerSubscriber } from '../logging/subcribers/file-logger.subscriber'
import { Logger } from '../logging/logger'
import type { Database } from '../data/database'
import { LogLevel } from '../common/enums/log-levell.enum'
import type { ReorderServiceProxy } from '../proxies/reorder-service.proxy'
import path = require('node:path')

const logFilePath = path.join(__dirname, '..', '..', 'logs', 'cardsLog.txt')

export class CardHandler extends SocketHandler {
  private logger: Logger = new Logger()
  private fileLoggerSubscriber: FileLoggerSubscriber = new FileLoggerSubscriber(logFilePath)
  private consoleLoggerSubscriber: ConsoleLoggerSubscriber = new ConsoleLoggerSubscriber()

  constructor(io: Server, db: Database, reorderService: ReorderServiceProxy) {
    super(io, db, reorderService)
    this.logger.subscribe(this.fileLoggerSubscriber)
    this.logger.subscribe(this.consoleLoggerSubscriber)
  }

  public handleConnection(socket: Socket): void {
    socket.on(CardEvent.CREATE, this.createCard.bind(this))
    socket.on(CardEvent.REORDER, this.reorderCards.bind(this))
    socket.on(CardEvent.DELETE, this.deleteCard.bind(this))
    socket.on(CardEvent.RENAME, this.renameCard.bind(this))
    socket.on(CardEvent.CHANGE_DESCRIPTION, this.changeDescription.bind(this))
    socket.on(CardEvent.DUPLICATE, this.duplicateCard.bind(this))
  }

  public createCard(listId: string, cardName: string): void {
    try {
      const newCard = new Card(cardName, '')
      const lists = this.db.getData()

      const updatedLists = lists.map(list => (list.id === listId ? list.setCards(list.cards.concat(newCard)) : list))

      this.db.setData(updatedLists)
      this.logger.log(LogLevel.INFO, `Card ${cardName} created in list ${listId}`)

      this.updateLists()
    } catch (error) {
      this.logger.log(LogLevel.ERROR, `Error creating card: ${error.message}`)
    }
  }

  private reorderCards({
    sourceIndex,
    destinationIndex,
    sourceListId,
    destinationListId
  }: {
    sourceIndex: number
    destinationIndex: number
    sourceListId: string
    destinationListId: string
  }): void {
    try {
      const lists = this.db.getData()
      const reordered = this.reorderService.reorderCards({
        lists,
        sourceIndex,
        destinationIndex,
        sourceListId,
        destinationListId
      })
      this.db.setData(reordered)
      this.logger.log(LogLevel.INFO, `Cards reordered in list ${sourceListId}`)

      this.updateLists()
    } catch (error) {
      this.logger.log(LogLevel.ERROR, `Error reordering cards: ${error.message}`)
    }
  }

  private deleteCard(listId: string, cardId: string): void {
    try {
      const lists = this.db.getData()

      const updatedLists = lists.map(list =>
        list.id === listId ? list.setCards(list.cards.filter(card => card.id !== cardId)) : list
      )

      this.db.setData(updatedLists)
      this.logger.log(LogLevel.INFO, `Card ${cardId} deleted from list ${listId}`)

      this.updateLists()
    } catch (error) {
      this.logger.log(LogLevel.ERROR, `Error deleting card: ${error.message}`)
    }
  }

  private renameCard({ listId, cardId, name }: { listId: string; cardId: string; name: string }): void {
    try {
      const lists = this.db.getData()

      const updatedLists = lists.map(list =>
        list.id === listId
          ? list.setCards(list.cards.map(card => (card.id === cardId ? ({ ...card, name } as Card) : card)))
          : list
      )

      this.db.setData(updatedLists)
      this.logger.log(LogLevel.INFO, `Card ${cardId} renamed in list ${listId}`)

      this.updateLists()
    } catch (error) {
      this.logger.log(LogLevel.ERROR, `Error renaming card: ${error.message}`)
    }
  }

  private changeDescription({
    listId,
    cardId,
    description
  }: {
    listId: string
    cardId: string
    description: string
  }): void {
    try {
      const lists = this.db.getData()

      const updatedLists = lists.map(list =>
        list.id === listId
          ? list.setCards(list.cards.map(card => (card.id === cardId ? ({ ...card, description } as Card) : card)))
          : list
      )

      this.db.setData(updatedLists)
      this.logger.log(LogLevel.INFO, `Card ${cardId} description changed in list ${listId}`)

      this.updateLists()
    } catch (error) {
      this.logger.log(LogLevel.ERROR, `Error changing card description: ${error.message}`)
    }
  }

  private duplicateCard(listId: string, cardId: string): void {
    try {
      const lists = this.db.getData()

      const listToUpdate = lists.find(list => list.id === listId)

      if (!listToUpdate) {
        return
      }

      const cardToDuplicate = listToUpdate.cards.find(card => card.id === cardId)

      if (!cardToDuplicate) {
        return
      }

      const duplicatedCard = cardToDuplicate.clone()

      const updatedLists = lists.map(list =>
        list.id === listId ? list.setCards(list.cards.concat(duplicatedCard)) : list
      )
      this.db.setData(updatedLists)
      this.logger.log(LogLevel.INFO, `Card ${cardId} duplicated in list ${listId}`)

      this.updateLists()
    } catch (error) {
      this.logger.log(LogLevel.ERROR, `Error duplicating card: ${error.message}`)
    }
  }
}
