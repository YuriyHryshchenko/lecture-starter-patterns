import type { ILogSubscriber } from '../types/logging.interface'
import { LogLevel } from '../../common/enums/log-levell.enum'

// PATTERN: Observer
class ConsoleLoggerSubscriber implements ILogSubscriber {
  public notify(level: LogLevel, message: string): void {
    if (level === LogLevel.ERROR) {
      console.error(`[${level}]: ${message}`)
    }
  }
}

export { ConsoleLoggerSubscriber }
