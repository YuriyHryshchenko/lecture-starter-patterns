import type { ILogSubscriber } from '../types/logging.interface'
import type { LogLevel } from '../../common/enums/log-levell.enum'
import * as fs from 'fs'
import * as path from 'node:path'

// PATTERN: Observer
class FileLoggerSubscriber implements ILogSubscriber {
  private readonly logFilePath: string

  constructor(logFilePath: string) {
    this.logFilePath = logFilePath
    this.ensureLogsDirectory()
  }

  private ensureLogsDirectory(): void {
    const logsDirectory = path.dirname(this.logFilePath)

    if (!fs.existsSync(logsDirectory)) {
      fs.mkdirSync(logsDirectory)
    }
  }

  public notify(level: LogLevel, message: string): void {
    const logEntry = `[${level}] ${new Date().toISOString()} - ${message}\n`

    fs.appendFile(this.logFilePath, logEntry, err => {
      if (err) {
        console.error('Error writing to the log file:', err)
      }
    })
  }
}

export { FileLoggerSubscriber }
