import type { LogLevel } from '../../common/enums/log-levell.enum'

export interface ILogSubscriber {
  notify(level: LogLevel, message: string): void
}
