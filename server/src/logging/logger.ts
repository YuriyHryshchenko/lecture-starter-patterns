import type { ILogSubscriber } from './types/logging.interface'
import type { LogLevel } from '../common/enums/log-levell.enum'

// PATTERN: Observer
class Logger {
  private subscribers: ILogSubscriber[] = []

  public subscribe(subscriber: ILogSubscriber): void {
    this.subscribers.push(subscriber)
  }

  public log(level: LogLevel, message: string): void {
    this.subscribers.forEach(subscriber => {
      subscriber.notify(level, message)
    })
  }
}

export { Logger }
