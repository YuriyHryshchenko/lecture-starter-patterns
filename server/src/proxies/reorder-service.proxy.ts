import { type ReorderService } from '../services/reorder.service'
import { type List } from '../data/models/list'
import type { IReorder } from '../common/types/reorder.interface'

//PATTERN: Proxy
class ReorderServiceProxy implements IReorder {
  private target: ReorderService

  constructor(target: ReorderService) {
    this.target = target
  }

  reorder<T>(items: T[], startIndex: number, endIndex: number): T[] {
    console.log(`ReorderService reorder method called with params: ${items}, ${startIndex}, ${endIndex}`)
    return this.target.reorder(items, startIndex, endIndex)
  }

  reorderCards({
    lists,
    sourceIndex,
    destinationIndex,
    sourceListId,
    destinationListId
  }: {
    lists: List[]
    sourceIndex: number
    destinationIndex: number
    sourceListId: string
    destinationListId: string
  }): List[] {
    console.log(
      `ReorderService reorderCards method called with params: ${JSON.stringify(lists)}, ${sourceIndex}, ${destinationIndex}, ${sourceListId}, ${destinationListId}`
    )
    return this.target.reorderCards({
      lists,
      sourceIndex,
      destinationIndex,
      sourceListId,
      destinationListId
    })
  }
}

export { ReorderServiceProxy }
