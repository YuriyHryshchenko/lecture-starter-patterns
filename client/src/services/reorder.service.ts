import type { DraggableLocation } from '@hello-pangea/dnd'

import { List } from '../common/types'
import { addCardToList, findListById, mapLists, removeCardFromList } from '../utilities/reorder-service.utilities'

export const reorderLists = (items: List[], startIndex: number, endIndex: number): List[] => {
  const clonedItems = [...items]
  const [removed] = clonedItems.splice(startIndex, 1)
  clonedItems.splice(endIndex, 0, removed)

  return clonedItems
}

export const reorderCards = (lists: List[], source: DraggableLocation, destination: DraggableLocation): List[] => {
  const { droppableId: sourceId, index: sourceIndex } = source
  const { droppableId: destinationId, index: destinationIndex } = destination

  const currentList = findListById(lists, sourceId)?.cards || []
  const nextList = findListById(lists, destinationId)?.cards || []
  const target = currentList[sourceIndex]

  const isMovingInSameList = sourceId === destinationId

  if (isMovingInSameList) {
    const clonedCurrentList = [...currentList]
    const [removed] = clonedCurrentList.splice(sourceIndex, 1)
    clonedCurrentList.splice(destinationIndex, 0, removed)
    const reorderedList = clonedCurrentList

    return mapLists(lists, list => (list.id === sourceId ? { ...list, cards: reorderedList } : list))
  }

  const newLists = mapLists(lists, list => {
    if (list.id === sourceId) {
      return {
        ...list,
        cards: removeCardFromList(currentList, sourceIndex)
      }
    }

    if (list.id === destinationId) {
      return {
        ...list,
        cards: addCardToList(nextList, destinationIndex, target)
      }
    }

    return list
  })

  return newLists
}
