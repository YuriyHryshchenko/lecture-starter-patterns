import { Card, List } from '../common/types'

export const findListById = (lists: List[], id: string): List | undefined => lists.find(list => list.id === id)

export const mapLists = <T>(list: T[], callback: (item: T) => T): T[] => list.map(callback)

export const removeCardFromList = (cards: Card[], index: number): Card[] => [
  ...cards.slice(0, index),
  ...cards.slice(index + 1)
]

export const addCardToList = (cards: Card[], index: number, card: Card): Card[] => [
  ...cards.slice(0, index),
  card,
  ...cards.slice(index)
]
