import type { DraggableProvided } from '@hello-pangea/dnd'

import type { Card } from '../../common/types'
import { CopyButton } from '../primitives/copy-button'
import { DeleteButton } from '../primitives/delete-button'
import { Splitter } from '../primitives/styled/splitter'
import { Text } from '../primitives/text'
import { Title } from '../primitives/title'
import { Container } from './styled/container'
import { Content } from './styled/content'
import { Footer } from './styled/footer'
import { socket } from '../../context/socket'
import { CardEvent } from '../../common/enums'

type Props = {
  card: Card
  listId: string
  isDragging: boolean
  provided: DraggableProvided
}

export const CardItem = ({ card, isDragging, provided, listId }: Props) => {
  return (
    <Container
      className="card-container"
      isDragging={isDragging}
      ref={provided.innerRef}
      {...provided.draggableProps}
      {...provided.dragHandleProps}
      data-is-dragging={isDragging}
      data-testid={card.id}
      aria-label={card.name}
    >
      <Content>
        <Title
          onChange={(name: string) => {
            socket.emit(CardEvent.RENAME, {
              listId: listId,
              cardId: card.id,
              name
            })
          }}
          title={card.name}
          fontSize="large"
          isBold
        />
        <Text
          text={card.description}
          onChange={(description: string) => {
            socket.emit(CardEvent.CHANGE_DESCRIPTION, {
              listId: listId,
              cardId: card.id,
              description
            })
          }}
        />
        <Footer>
          <DeleteButton
            onClick={() => {
              socket.emit(CardEvent.DELETE, listId, card.id)
            }}
          />
          <Splitter />
          <CopyButton
            onClick={() => {
              socket.emit(CardEvent.DUPLICATE, listId, card.id)
            }}
          />
        </Footer>
      </Content>
    </Container>
  )
}
